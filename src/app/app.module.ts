import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MedicosComponent } from './unitarias-intermedio/espias/medicos.component';
import { MedicoComponent } from './integracion/medico/medico.component';
import { HospitalComponent } from './integracion/hospital/hospital.component';
import { IncrementadorComponent } from './integracion/incrementador/incrementador.component';
import { RouterModule } from '@angular/router';
import { RUTAS } from './integracion/avanzadas/rutas/app.routes';
import { NavbarComponent } from './integracion/avanzadas/navbar/navbar.component';
import { RouterMedicoComponent } from './integracion/avanzadas/router-medico/router-medico.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    MedicosComponent,
    MedicoComponent,
    HospitalComponent,
    IncrementadorComponent,
    NavbarComponent,
    RouterMedicoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(RUTAS)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
