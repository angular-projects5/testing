import { EventEmitter } from '@angular/core';

export class Jugador2 {
    healthPoints: number;
    healthPointsCambia = new EventEmitter<number>();

    constructor() {
        this.healthPoints = 100;
    }

    recibeDanio(danio: number) {
        if (danio >= this.healthPoints) {
            this.healthPoints = 0;
        } else {
            this.healthPoints = this.healthPoints - danio;
        }

        this.healthPointsCambia.emit(this.healthPoints);
    }

}
