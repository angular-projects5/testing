import { Jugador2 } from './jugador2';


describe('Pruebas EventEmitter', () => {
    let jugador: Jugador2;

    beforeEach(() => jugador = new Jugador2());

    // espera a que se realice el subscribe antes de acabar la prueba.
    it('Debe de emitir un evento cuando recibe daño', () => {

        let nuevoHP = 0;

        jugador.healthPointsCambia.subscribe(hp => {
            nuevoHP = hp;
        });

        jugador.recibeDanio(1000);

        expect(nuevoHP).toBe(0);
    });

    it('Debe de emitir un evento cuando recibe daño y sobrevivir si es menor que 100', () => {

        let nuevoHP = 0;

        jugador.healthPointsCambia.subscribe(hp => {
            nuevoHP = hp;
        });

        jugador.recibeDanio(50);

        expect(nuevoHP).toBe(50);
    });
});
