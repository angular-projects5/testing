import { MedicosComponent } from './medicos.component';
import { MedicosService } from './medicos.service';
import { from, throwError } from 'rxjs';


describe('MedicosComponent', () => {

    let componente: MedicosComponent;
    const servicio = new MedicosService(null);

    beforeEach(() => {
        componente = new MedicosComponent(servicio);
    });


    it('Init: Debe de cargar los médicos', () => {
        const medicos = ['medico1', 'medico2', 'medico3'];

        spyOn(servicio, 'getMedicos').and.callFake(() => {
            return from([medicos]);
        });

        componente.ngOnInit();

        expect(componente.medicos.length).toBeGreaterThan(0);

    });

    it('Cuando llamamos agregar médico del componente, tiene que llamar al método del servidor agregar médico', () => {

        const espia = spyOn(servicio, 'agregarMedico').and.callFake(() => {
            return from([]);
        });

        componente.agregarMedico();

        expect(espia).toHaveBeenCalled();

    });

    it('Debe de agregar un nuevo médico al arreglo de médicos', () => {

        const medicoBBDD = { id: 1, nombre: 'David' };

        // hay que retornar un observable porque es lo que devuelve la fonción agregar médico del servicio
        const espia = spyOn(servicio, 'agregarMedico').and.returnValue(from([medicoBBDD]));

        componente.agregarMedico();

        expect(componente.medicos.indexOf(medicoBBDD)).toBeGreaterThanOrEqual(0);

    });

    it('Si falla al agregar, la propiedad del mensajeError debe ser igual al error del servicio', () => {

        const miError = 'Fallo al crear el médico';

        // hay que retornar un observable porque es lo que devuelve la fonción agregar médico del servicio
        const espia = spyOn(servicio, 'agregarMedico').and
            .returnValue(throwError(miError));

        componente.agregarMedico();

        expect(componente.mensajeError).toBe(miError);

    });

    it('Debe de llamar al servidor para borrar un médico', () => {

        spyOn(window, 'confirm').and.returnValue(true);

        const parametro = '1';

        // hay que retornar un observable porque es lo que devuelve la fonción agregar médico del servicio
        const espia = spyOn(servicio, 'borrarMedico').and
            .returnValue(from([]));

        componente.borrarMedico(parametro);

        expect(espia).toHaveBeenCalledWith(parametro);

    });


    it('No debe de llamar al servidor para borrar un médico si cancela', () => {

        spyOn(window, 'confirm').and.returnValue(false);

        const parametro = '1';

        // hay que retornar un observable porque es lo que devuelve la fonción agregar médico del servicio
        const espia = spyOn(servicio, 'borrarMedico').and
            .returnValue(from([]));

        componente.borrarMedico(parametro);

        expect(espia).not.toHaveBeenCalledWith(parametro);

    });


});
