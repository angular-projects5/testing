// partes generales de las pruebas en jasmine
// describe('Pruebas de strings'){
//     it('Debe de regresar un string')
// }
// el puerto de los tests es el 9876


import { mensaje } from './string';

// con una x delante de describe o it, omite la prueba
describe('Pruebas de strings', () => {
    it('Debe de regresar un string', () => {
        const resp = mensaje('David');

        expect(typeof resp).toBe('string');
    });

    it('Debe de devolver un saludo con el nombre enviado', () => {
        const nombre = 'David';

        const resp = mensaje(nombre);

        expect(resp).toContain(nombre);
    });
});
