import { incrementar } from './numeros';

describe('pruebas de números', () => {
    it('Debe de devolver 100 si el número es mayor que 100', () => {
        const resp = incrementar(300);

        expect(resp).toBe(100);

    });


    it('Debe de devolver num + 1 si el número no es mayor que 100', () => {
        const resp = incrementar(50);

        expect(resp).toBe(51);

    });
});
