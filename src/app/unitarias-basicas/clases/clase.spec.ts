import { Jugador } from './clase';

// ciclos de vida de las pruebas
// beforeAll()
// beforeEach()
// afterAll()
// afterEach()

// para ver la cobertura de los tests (el porcentaje que tenemos cubierto):
// ejecutar 'ng tests --code-coverage'
// esto genera una carpeta coverage/nameapp
// en esta ruta se encuentra un fichero index.html que contiene el nivel de las pruebas cubierto
// ruta: /coverage/nameapp/index.html (lo aceptable es un 60 ó 70 % testado)
// Te dice las líneas testadas y clicando en los links, puedes ir viendo qué te falta por testar.

describe('Pruebas de clase', () => {
    const jugador = new Jugador();

    beforeAll(() => {
        console.log('beforeAll');

    });
    beforeEach(() => {
        console.log('beforeEach');
        jugador.healthPoints = 100;

    });
    afterAll(() => {
        console.log('afterAll');

    });
    afterEach(() => {
        console.log('afterEach');

    });

    it('Debe de retornar 80 de healhPoints si recibe 20 de daño', () => {
        const resp = jugador.recibeDanio(20);

        expect(resp).toBe(80);
    });

    it('Debe de retornar 50 de healhPoints si recibe 50 de daño', () => {
        console.log('esta prueba ya no va a fallar');

        const resp = jugador.recibeDanio(50);

        expect(resp).toBe(50);
    });

    it('Debe de retornar 0 de healhPoints si recibe 100 de daño o más', () => {
        console.log('esta prueba ya no va a fallar');

        const resp = jugador.recibeDanio(100);

        expect(resp).toBe(0);
    });
});
