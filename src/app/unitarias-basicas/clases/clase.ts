export class Jugador {
    healthPoints: number;

    constructor() {
        this.healthPoints = 100;
    }

    recibeDanio(danio: number) {
        if (danio >= this.healthPoints) {
            this.healthPoints = 0;
        } else {
            this.healthPoints = this.healthPoints - danio;
        }

        return this.healthPoints;
    }

}
