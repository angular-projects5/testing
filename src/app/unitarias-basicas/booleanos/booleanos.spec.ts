import { usuarioIngresado } from './booleanos';

describe('Pruebas de booleanos', () => {
    it('Debe de devolver true', () => {
        const resp = usuarioIngresado();

        expect(resp).toBeTruthy();
    });
});
