import { TestBed, ComponentFixture } from '@angular/core/testing';
import { IncrementadorComponent } from './incrementador.component';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';


describe('Incrementador Component', () => {

    let component: IncrementadorComponent;
    let fixture: ComponentFixture<IncrementadorComponent>;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [IncrementadorComponent],
            // importado el FormsModule para poder usar el ngmodel y esas cosas
            imports: [FormsModule]
        });

        fixture = TestBed.createComponent(IncrementadorComponent);
        component = fixture.componentInstance;

    });

    // Esto no pasa las pruebas por que escribimos leyenda en el componente, pero no se actualiza en la vista
    // Para eso hay que lanzar la detección de cambios.
    // it('Debe de mostrar la leyenda en el HTML', () => {

    //     component.leyenda = 'Progreso de carga';

    //     // queryAll devuelve todos y queryNodes los nodos.
    //     // El by te permite buscar por directiva, por css, por query, por id...
    //     const elem: HTMLElement = fixture.debugElement.query(By.css('h3')).nativeElement;

    //     expect(elem.innerHTML).toContain('Progreso de carga');
    // });

    it('Debe de mostrar la leyenda en el HTML', () => {

        component.leyenda = 'Progreso de carga';

        fixture.detectChanges(); // disparar la detección de cambios

        // queryAll devuelve todos y queryNodes los nodos.
        // El by te permite buscar por directiva, por css, por query, por id...
        const elem: HTMLElement = fixture.debugElement.query(By.css('h3')).nativeElement;

        expect(elem.innerHTML).toContain('Progreso de carga');
    });

    it('Debe de mostrar en el input el valor del progreso', (done) => {

        component.cambiarValor(5);
        fixture.detectChanges();

        // para asegurarnos de que se llama cuando acaba la detección de cambios, podemos disparar una promesa
        fixture.whenStable().then(() => {

            const input = fixture.debugElement.query(By.css('input'));
            const elem = input.nativeElement;

            console.log(elem);


            expect(elem.value).toBe('55');
            done();

        });
    });

    it('Debe de incrementar/decrementar en 5, con un click en el botón', () => {

        // hacer referencia a los botones
        const botones = fixture.debugElement.queryAll(By.css('.btn-primary'));

        console.log(botones);

        // simulamos click del botón
        botones[0].triggerEventHandler('click', null);
        expect(component.progreso).toBe(45);

        botones[1].triggerEventHandler('click', null);
        expect(component.progreso).toBe(50);
    });

    it('Debe de cambiar el progreso por pantalla en el título del componente cuando clicamos en algún botón', () => {

        // hacer referencia a los botones
        const botones = fixture.debugElement.queryAll(By.css('.btn-primary'));

        // simulamos click del botón
        botones[0].triggerEventHandler('click', null);

        // Para detectar los cambios en la vista.
        fixture.detectChanges();

        const elem: HTMLElement = fixture.debugElement.query(By.css('h3')).nativeElement;

        expect(elem.innerHTML).toContain('45');
    });

});
