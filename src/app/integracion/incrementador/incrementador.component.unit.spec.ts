import { IncrementadorComponent } from './incrementador.component';

describe('Incrementador component unit tests', () => {

    let component: IncrementadorComponent;

    beforeEach(() => component = new IncrementadorComponent());

    it('Prueba unitaria separada de la de integración', () => {
        component.progreso = 50;
        component.cambiarValor(5);

        expect(component.progreso).toBe(55);
    });
});