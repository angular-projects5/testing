import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RouterMedicoComponent } from './router-medico.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of, Subject } from 'rxjs';


class FakeRouter {
  navigate(params) { }
}

class FakeActivatedRouter {
  // params: Observable<any> = of();

  private subject = new Subject();

  get params() {
    return this.subject.asObservable();
  }

  push(valor) {
    this.subject.next(valor);
  }


}

describe('RouterMedicoComponent', () => {
  let component: RouterMedicoComponent;
  let fixture: ComponentFixture<RouterMedicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RouterMedicoComponent],
      providers: [
        // En vez de decirle que use el router, le decimos que utilice ese servicio fake
        // Router,
        { provide: Router, useClass: FakeRouter },
        // ActivatedRoute
        { provide: ActivatedRoute, useClass: FakeActivatedRouter }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RouterMedicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Debe de redireccionar a médico cuando se guarde', () => {
    const router = TestBed.inject(Router);
    const spy = spyOn(router, 'navigate');

    component.guardarMedico();

    expect(spy).toHaveBeenCalledWith(['medico', '123']);
  });

  it('Debe de colocar el id nuevo', () => {
    component = fixture.componentInstance;
    const activatedRoute: FakeActivatedRouter = TestBed.get(ActivatedRoute);

    activatedRoute.push({ id: 'nuevo' });

    expect(component.id).toBe('nuevo');

  });
});
