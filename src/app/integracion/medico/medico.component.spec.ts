import { MedicoComponent } from './medico.component';

// Clase testbed para poder hacer los test de integración
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { MedicoService } from './medico.service';
import { HttpClientModule } from '@angular/common/http';

describe('Medico component', () => {
    let component: MedicoComponent;
    let componentFixture: ComponentFixture<MedicoComponent>;

    // Para las pruebas unitarias se hace así, pero ahora para las de integración no.
    // beforeEach( () => {

    //     component = new MedicoComponent();
    // });

    // En las pruebas de integración, se suele configurar aquí el testbed.
    beforeEach(() => {

        // Hay que poner lo que necesita este componente para probarse
        TestBed.configureTestingModule({
            declarations: [MedicoComponent],
            // si hubiese servicios de los que depende este componente, se pondrían en los providers.
            // servicios que no tengan en el decorador el providerIn: 'root', si tienen ese providerIn, ya se unen sólos
            providers: [MedicoService],
            // Si hubiese módulos de los que dependiese, se pondría en los imports
            imports: [HttpClientModule]
        });

        // Una vez configurado, necesitamos crear un componente compilado y procesado por el TestBed
        // el createComponent genera un ComponentFixture, este nos permite acceder al html y otras cosas más
        componentFixture = TestBed.createComponent(MedicoComponent);
        component = componentFixture.componentInstance;

    });

    it('Debe de crearse el componente correctamente', () => {
        expect(component).toBeTruthy();
    });

    it('Debe devolver el nombre del médico', () => {
        const nombre = 'David';
        const res = component.saludarMedico(nombre);

        expect(res).toContain(nombre);
    });
});
